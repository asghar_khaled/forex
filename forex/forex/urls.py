"""forex URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from . import settings
from apscheduler.scheduler import Scheduler
from myweb import converter


urlpatterns = [
    url(r'^myapp/', include('myapp.urls', namespace='myapp-urls', app_name='myapp')),
    url(r'^myweb/', include('myweb.urls', namespace='myweb-urls', app_name='myweb')),
    url(r'^django_auth/', include('django_auth.urls', namespace='django_auth-urls',
                                  app_name='django_auth')),
    url(r'^superadmin/', include('superadmin.urls', namespace='superadmin-urls',
                                  app_name='superadmin')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


sched = Scheduler()
sched.start()


@sched.interval_schedule(seconds=3600)
def some_decorated_task():
    converter.scheduler_function()