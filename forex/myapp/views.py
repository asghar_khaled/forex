from django.db.models import Q
from django.shortcuts import render
from rest_framework import generics, response, status, exceptions
from . import serializers, models, utils
from rest_framework.permissions import IsAuthenticated
from forex import settings
import razorpay
from django.db import transaction
import hashlib
import hmac
from django.core.exceptions import ObjectDoesNotExist
from threading import Thread



# Create your views here.


class ListDetails(generics.GenericAPIView):
    """
    use this endpoint to list purpose, travellers count, product, state and branch
    """

    def get(self, request):
        data = dict()
        data['purpose'] = models.Purpose.objects.all().order_by('id').values("id", "name")
        data['travellers_count'] = models.TravellersCount.objects.all().order_by('id').values("id", "name")
        data['product'] = models.Product.objects.all().order_by('id').values("id", "name")
        data['currency'] = models.Currencies.objects.all().order_by('id').values("id", "name")
        data['branch'] = models.Branch.objects.all().order_by('id').values("id", "name")
        return response.Response(data)


class ListBranchAddress(generics.GenericAPIView):

    """
    use this endpoint to list branch address
    """

    def get(self, request):
        data = dict()
        data['address'] = models.Branch.objects.all().order_by('id').values("id", "address", "phone1", "phone2")
        return response.Response(data)


class ListCurrencyForProduct(generics.GenericAPIView):

    def get(self, request):
        import pdb
        data = dict()
        qs = models.Currencies.objects.all()

        for obj in qs:
            if (obj.id==8):
                obj.value = round(obj.value + 0.01, 2)
            else:
                obj.value = round(obj.value + 01.00, 2)
        data["card"] = serializers.ListCurrencySerializer(qs, many=True).data

        for obj1 in qs:
            obj1.value = obj1.value
        data["cheque"] = serializers.ListCurrencySerializer(qs, many=True).data

        for obj2 in qs:
            if (obj2.id==8):
                obj2.value = round(obj2.value + 0.01, 2)
            else:
                obj2.value = obj2.value + 00.50

        data["cash"] = serializers.ListCurrencySerializer(qs, many=True).data
        return response.Response(data)


class ListCurrencyView(generics.ListAPIView):

    serializer_class = serializers.ListCurrencySerializer
    queryset = models.Currencies.objects.all()


class CreateForexView(generics.CreateAPIView):
    """
    use this endpoint to Buy Forex
    """
    permission_classes = (
        IsAuthenticated,
    )

    serializer_class = serializers.CreateForexSerializer
    queryset = models.Forex.objects.all()


class ListDetailsSellView(generics.GenericAPIView):

    def get(self, request):
        data = dict()
        data['currency'] = models.Currencies.objects.all().order_by('id').values("id", "name",
                                                                                 "value")
        data['product'] = models.Product.objects.filter(~Q(id=1)).order_by('id').values("id",
                                                                                        "name")
        return response.Response(data)


class ListCurrencyForSellProduct(generics.GenericAPIView):

    def get(self, request):
        data = dict()
        qs = models.Currencies.objects.all()

        for obj in qs:
            if (obj.id==8):
                obj.value = round(obj.value - 0.01, 2)
            else:
                obj.value = round(obj.value - 01.00, 2)
        data["card"] = serializers.ListCurrencySerializer(qs, many=True).data

        for obj1 in qs:
            obj1.value = obj1.value
        data["cheque"] = serializers.ListCurrencySerializer(qs, many=True).data

        for obj2 in qs:
            if (obj2.id==8):
                obj2.value = round(obj2.value - 0.01, 2)
            else:
                obj2.value = round(obj2.value - 00.50, 2)

        data["cash"] = serializers.ListCurrencySerializer(qs, many=True).data
        return response.Response(data)


class CreateForexSellView(generics.CreateAPIView):
    """
    use this endpoint to create sell forex
    """

    permission_classes = (
        IsAuthenticated,
    )

    serializer_class = serializers.CreateForexSellSerializer
    queryset = models.SellForex.objects.all()


class CreateMoneyTransferView(generics.CreateAPIView):

    serializer_class = serializers.CreateMoneyTransferSerializer
    queryset = models.MoneyTransfer.objects.all()


class CreateReloadForexView(generics.CreateAPIView):

    serializer_class = serializers.CreateReloadForexSerializer
    queryset = models.ReloadForex.objects.all()


class PaymentGatewayView(generics.GenericAPIView):
    """
       use this endpoint enter into payment gateway
    """


    def get(self, request, pk):
        order_obj = utils.int_to_order(pk)

        if hasattr(order_obj, "is_paid"):
            if order_obj.is_paid:
                raise exceptions.ValidationError({"non_field_errors":
                                                      ["Already paid for this booking"]})
        client = razorpay.Client(auth=(settings.YOUR_API_KEY, settings.YOUR_API_SECRET))
        resp = client.order.create(data={'currency': 'INR', 'receipt': '123456', 'payment_capture': True,
                                         'amount': int(order_obj.inr_amount) * 100 })
        models.ForexPayment.objects.update_or_create(forex=order_obj, defaults={'forex': order_obj, 'txn_id': resp['id'],
                                                                               'amount': int(order_obj.inr_amount),
                                                                               'is_success': False})
        return response.Response({'order_id': resp['id'], 'amount': resp['amount'], })


class PaymentSuccessView(generics.GenericAPIView):
    """
    use this endpoint to return payment success manually
    """
    permission_classes = (
        IsAuthenticated,
    )
    serializer_class = serializers.PaymentSuccessSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            return self.payment_success(serializer)
        else:
            raise exceptions.ValidationError(serializer.errors)

    @transaction.atomic()
    def payment_success(self, serializer):
        payment_info = self.get_order(serializer.data['razorpay_order_id'])
        string = str(serializer.data['razorpay_order_id']) + "|" + str(serializer.data['razorpay_payment_id'])
        dig = hmac.new(settings.YOUR_API_SECRET.encode('utf-8'), msg=string.encode('utf-8'),
                       digestmod=hashlib.sha256).hexdigest()

        if str(dig) == str(serializer.data['razorpay_signature']):
            payment_info.is_success = True
            payment_info.save()
            payment_info.forex.is_paid = True
            payment_info.forex.save()
            print(payment_info.forex.inr_amount, payment_info.forex.foreign_amount)
            # payment_info.order.payment_type = 'ON'
            # payment_info.order.save()
            # payment_info.order.orderdetails.status = utils.int_to_status(5)
            # payment_info.order.orderdetails.save()
            Thread(target=utils.SendMessageForOrders.send_buy_forex_user,
                   args=(payment_info, )).start()
            Thread(target=utils.SendMessageForOrders.send_buy_forex_branch_admin,
                   args=(payment_info, )).start()
            return response.Response({'data': 'success'})
        else:
            raise exceptions.ValidationError({"non_field_errors": ["Signature mismatch."]})

    @staticmethod
    def get_order(txn_id):
        try:
            return models.ForexPayment.objects.get(txn_id=txn_id)
        except ObjectDoesNotExist:
            raise exceptions.ValidationError({"non_field_errors": "Invalid Object"})





