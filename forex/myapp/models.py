from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Purpose(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        db_table = "purpose"


class TravellersCount(models.Model):
    name = models.IntegerField()

    class Meta:
        db_table = "travellers_count"


class Product(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        db_table = "product"


class Currencies(models.Model):
    name = models.CharField(max_length=255)
    value = models.FloatField()
    code = models.CharField(max_length=255)

    class Meta:
        db_table = "currencies"


class State(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        db_table = "state"


class Branch(models.Model):
    state = models.ForeignKey(State)
    address = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    phone1 = models.CharField(max_length=15)
    phone2 = models.CharField(max_length=15,null=True)
    location = models.CharField(max_length=255)

    class Meta:
        db_table = "branch"


class Forex(models.Model):
    #for buy forex
    user = models.ForeignKey(User)
    purpose = models.ForeignKey(Purpose)
    product = models.ForeignKey(Product)
    travellers = models.ForeignKey(TravellersCount)
    travel_date = models.DateField()
    currency = models.ForeignKey(Currencies)
    branch = models.ForeignKey(Branch)
    inr_amount = models.FloatField() # amount to be paid + tax
    full_amount = models.FloatField()
    foreign_amount = models.FloatField()
    payment_method = models.BooleanField() # 0 Advance  1 -  Full amount
    payment_place = models.BooleanField()  # 0 - home  1 - branch
    delivery_address = models.CharField(max_length=255)
    is_paid = models.BooleanField(default=0)
    created_on = models.DateTimeField(auto_now_add=True)
    forex_details_completed = models.BooleanField(default=0)

    class Meta:
        db_table = "forex"


class ForexDetails(models.Model):
    forex = models.OneToOneField(Forex)
    is_contacted = models.BooleanField(default=0)
    is_business = models.BooleanField(default=0)

    class Meta:
        db_table = "forex_details"


class ForexPayment(models.Model):
    forex = models.ForeignKey(Forex)
    txn_id = models.CharField(max_length=255)
    amount = models.IntegerField()
    is_success = models.BooleanField()
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "forex_payment"


class SellForex(models.Model):
    user = models.ForeignKey(User)
    currency = models.ForeignKey(Currencies)
    product = models.ForeignKey(Product)
    foreign_amount = models.FloatField()
    inr_amount = models.FloatField()
    branch = models.ForeignKey(Branch)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "sell_forex"


class MoneyTransfer(models.Model):
    name = models.CharField(max_length=54)
    mobile = models.CharField(max_length=10)
    branch = models.ForeignKey(Branch)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "money_transfer"


class ReloadForex(models.Model):
    name = models.CharField(max_length=54)
    mobile = models.CharField(max_length=10)
    branch = models.ForeignKey(Branch)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "reload_forex"



class BookingStatus(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        db_table = 'booking_status'





