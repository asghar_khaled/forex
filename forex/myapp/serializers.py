from rest_framework import serializers
from . import models, utils
from threading import Thread


class ListCurrencySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Currencies
        fields = ('id', 'code', 'value',)


class CreateForexSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Forex
        fields = ('id', 'travel_date', 'inr_amount','full_amount', 'foreign_amount', 'payment_method',
                  'payment_place', 'branch', 'currency', 'purpose', 'travellers',
                  'delivery_address','product')

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        validated_data['inr_amount'] = round(validated_data['inr_amount'])
        validated_data['full_amount'] = round(validated_data['full_amount'])
        validated_data['foreign_amount'] = round(validated_data['foreign_amount'])
        print(validated_data['inr_amount'], validated_data['foreign_amount'])
        forex = models.Forex.objects.create(**validated_data)
        return forex


class CreateForexSellSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.SellForex
        fields = ('id', 'foreign_amount', 'inr_amount', 'branch', 'currency', 'product')

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        validated_data['inr_amount'] = round(validated_data['inr_amount'])
        validated_data['foreign_amount'] = round(validated_data['foreign_amount'])
        forex = models.SellForex.objects.create(**validated_data)
        Thread(target=utils.SendMessageForOrders.send_sell_forex_user, args=(forex,)).start()
        Thread(target=utils.SendMessageForOrders.send_sell_forex_branch_admin,
               args=(forex,)).start()
        return forex


class CreateMoneyTransferSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.MoneyTransfer
        fields = ('id', 'mobile','branch','name',)

    def create(self, validated_data):
        money_transfer = models.MoneyTransfer.objects.create(**validated_data)
        #Thread(target=utils.SendMoneyTransferLocation.send_location, args=(money_transfer,)).start()
        return money_transfer


class CreateReloadForexSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ReloadForex
        fields = ('id', 'mobile','branch','name',)


class PaymentSuccessSerializer(serializers.Serializer):
    razorpay_order_id = serializers.CharField()
    razorpay_payment_id = serializers.CharField()
    razorpay_signature = serializers.CharField()

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

