# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-28 12:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0025_moneytransfer'),
    ]

    operations = [
        migrations.CreateModel(
            name='ForexDetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_contacted', models.BooleanField(default=0)),
                ('is_business', models.BooleanField(default=0)),
                ('forex', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='myapp.Forex')),
            ],
            options={
                'db_table': 'forex_details',
            },
        ),
    ]
