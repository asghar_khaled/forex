# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-04 15:19
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0030_forex_forex_details_completed'),
    ]

    operations = [
        migrations.AddField(
            model_name='forex',
            name='product',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='myapp.Product'),
            preserve_default=False,
        ),
    ]
