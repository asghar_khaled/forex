# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-22 14:02
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('myapp', '0022_forex_delivery_address'),
    ]

    operations = [
        migrations.CreateModel(
            name='SellForex',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('foreign_amount', models.FloatField()),
                ('inr_amount', models.FloatField()),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('branch', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='myapp.Branch')),
                ('currency', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='myapp.Currencies')),
                ('purpose', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='myapp.Purpose')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'sell_forex',
            },
        ),
    ]
