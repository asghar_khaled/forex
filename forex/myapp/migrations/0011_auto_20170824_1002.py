# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-24 10:02
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('myapp', '0010_role_userrole'),
    ]

    operations = [
        migrations.CreateModel(
            name='Forex',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('travel_date', models.DateField()),
                ('inr_amount', models.FloatField()),
                ('foreign_amount', models.FloatField()),
                ('payment_method', models.BooleanField()),
                ('payment_place', models.BooleanField()),
                ('is_paid', models.BooleanField(default=0)),
                ('branch', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='myapp.Branch')),
                ('currency', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='myapp.Currency')),
                ('purpose', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='myapp.Purpose')),
                ('state', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='myapp.State')),
                ('travellers', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='myapp.TravellersCount')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'forex',
            },
        ),
        migrations.RemoveField(
            model_name='userrole',
            name='role',
        ),
        migrations.RemoveField(
            model_name='userrole',
            name='user',
        ),
        migrations.DeleteModel(
            name='Role',
        ),
        migrations.DeleteModel(
            name='UserRole',
        ),
    ]
