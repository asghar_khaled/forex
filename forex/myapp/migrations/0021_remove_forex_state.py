# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-12 12:01
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0020_auto_20170912_0536'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='forex',
            name='state',
        ),
    ]
