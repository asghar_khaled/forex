import urllib.parse
import urllib.request
from . import models
from rest_framework import exceptions
from django.core.exceptions import ObjectDoesNotExist
from django_auth.models import AdminRole


class SendMessageForOrders(object):

    @staticmethod
    def send_buy_forex_user(payment_info):
        buy_forex = payment_info.forex
        order_id = buy_forex.id
        paid_amount = round(buy_forex.inr_amount)
        foreign_amount = round(buy_forex.foreign_amount)
        currency_code = buy_forex.currency.code
        user_mobile = buy_forex.user.username
        location = buy_forex.branch.location
        values = {
            'uname': "Indiacements",
            'pass' : "FFMCtran",
            'send': "ICCAPS",
            'dest': int(user_mobile),
            'msg': "Thank you for placing a buy order(id-%s) for %s %s.You have paid Rs.%s."
                   "Our branch will contact you for further verification."
                   "Visit our branch : %s" %(order_id, foreign_amount, currency_code,
                                           paid_amount, location)

        }
        url = "http://api.unicel.in/SendSMS/sendmsg.php"
        postdata = urllib.parse.urlencode(values).encode('utf-8')
        req = urllib.request.Request(url, postdata)
        urllib.request.urlopen(req)

    @staticmethod
    def send_buy_forex_branch_admin(payment_info):
        branch_admin = AdminRole.objects.get(branch=payment_info.forex.branch.id)
        branch_admin_mobile = branch_admin.user.username
        forex = payment_info.forex
        buy_forex_order_id = forex.id
        currency_code = forex.currency.code
        customer_name = forex.user.first_name
        customer_mobile = forex.user.username
        paid_amount = round(forex.inr_amount)
        fx_amount = round(forex.foreign_amount)
        print(paid_amount, fx_amount)
        values = {
            'uname': "Indiacements",
            'pass': "FFMCtran",
            'send': "ICCAPS",
            'dest': int(branch_admin_mobile),
            'msg': "Buy Order! \n"
                   "Order id : %s \n"
                   "Name : %s \n"
                   "Contact : %s \n"
                   "Req. Foreign Currency : %s %s \n"
                   "Amount Paid : Rs. %s" %(buy_forex_order_id, customer_name,
                                            customer_mobile, fx_amount,
                                            currency_code, paid_amount)
        }
        url = "http://api.unicel.in/SendSMS/sendmsg.php"
        postdata = urllib.parse.urlencode(values).encode('utf-8')
        req = urllib.request.Request(url, postdata)
        urllib.request.urlopen(req)


    @staticmethod
    def send_sell_forex_user(sell_forex):
        user_mobile = sell_forex.user.username
        order_id = sell_forex.id
        foreign_amount = sell_forex.foreign_amount
        currency_code = sell_forex.currency.code
        location = sell_forex.branch.location

        values = {
            'uname': "Indiacements",
            'pass': "FFMCtran",
            'send': "ICCAPS",
            'dest': int(user_mobile),
            'msg': "Thank you for placing sell order(id-%s) for %s %s."
                   "Our branch will contact you for further verification."
                   "Visit our branch : %s"
                   %(order_id, foreign_amount, currency_code, location)

        }
        url = "http://api.unicel.in/SendSMS/sendmsg.php"
        postdata = urllib.parse.urlencode(values).encode('utf-8')
        req = urllib.request.Request(url, postdata)
        urllib.request.urlopen(req)


    @staticmethod
    def send_sell_forex_branch_admin(sell_forex):
        branch_admin = AdminRole.objects.get(branch=sell_forex.branch.id)
        branch_admin_mobile = branch_admin.user.username
        sell_forex_order_id = sell_forex.id
        customer_name = sell_forex.user.first_name
        customer_mobile = sell_forex.user.username
        inr_amount = sell_forex.inr_amount
        fx_amount = sell_forex.foreign_amount
        currency_code = sell_forex.currency.code
        print(branch_admin_mobile)
        values = {
            'uname': "Indiacements",
            'pass': "FFMCtran",
            'send': "ICCAPS",
            'dest': int(branch_admin_mobile),
            'msg': "Sell Order! \n"
                   "Order id : %s \n"
                   "Name : %s \n"
                   "Contact : %s \n"
                   "Foreign Currency : %s %s \n"
                   "INR Amount : Rs. %s" % (sell_forex_order_id, customer_name,
                                            customer_mobile, fx_amount,
                                             currency_code, inr_amount)
        }
        url = "http://api.unicel.in/SendSMS/sendmsg.php"
        postdata = urllib.parse.urlencode(values).encode('utf-8')
        req = urllib.request.Request(url, postdata)
        urllib.request.urlopen(req)

    @staticmethod
    def send_location(money_transfer):
        branch = models.Branch.objects.get(id=money_transfer.branch.id)
        location = branch.location
        values = {
            'uname': "Indiacements",
            'pass': "FFMCtran",
            'send': "ICCAPS",
            'dest': int(money_transfer.mobile),
            'msg': "visit our nearest branch : %s" % location

        }
        url = "http://api.unicel.in/SendSMS/sendmsg.php"
        postdata = urllib.parse.urlencode(values).encode('utf-8')
        req = urllib.request.Request(url, postdata)
        urllib.request.urlopen(req)


def int_to_order(pk):
    try:
        return models.Forex.objects.get(pk=pk)
    except models.Forex.DoesNotExist:
        raise exceptions.ValidationError({"non_field_errors": "Invalid object" })


def int_to_status(pk):
    try:
        return models.BookingStatus.objects.get(pk=pk)
    except ObjectDoesNotExist:
        raise exceptions.ValidationError({"non_field_errors": "Invalid object" })