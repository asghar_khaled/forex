from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^list/currency/$', views.ListCurrencyView.as_view(), name='list-currency'),
    url(r'^list/branch/$', views.ListBranchAddress.as_view(), name='list-branch'),
    url(r'^create/forex/$', views.CreateForexView.as_view(), name='create-forex'),
    url(r'^list/details/$', views.ListDetails.as_view(), name='list-details'),
    url(r'^list/currency/product/$', views.ListCurrencyForProduct.as_view(),
        name='list-currency-product'),
    url(r'^list/currency/product/sell/$', views.ListCurrencyForSellProduct.as_view(),
        name='list-currency-product'),
    url(r'^list/details/sell/$', views.ListDetailsSellView.as_view(), name='list-details-sell'),
    url(r'^create/forex/sell/$', views.CreateForexSellView.as_view(), name='create-forex-sell'),
    url(r'^create/money/transfer/$', views.CreateMoneyTransferView.as_view(),
        name='create-money-transfer'),
    url(r'^create/reload/forex/$', views.CreateReloadForexView.as_view(),
        name='create-reload-forex'),
    url(r'^create/forex/payment/(?P<pk>[0-9]+)/$', views.PaymentGatewayView.as_view(),
        name='create-forex-payment'),
    url(r'^forex/payment/success/$', views.PaymentSuccessView.as_view(),
        name='forex_payment_success'),

]

