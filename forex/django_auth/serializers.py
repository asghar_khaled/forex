from rest_framework import serializers, exceptions
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from . import models, constants, utils
from django.db import transaction
from annoying.functions import get_object_or_None
from threading import Thread

class UserRoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.UserRole
        fields = ("id", "role")


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.UserProfile
        fields = ("id", "passport_no", "aadhaar_no",)


class ObtainTokenSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()
    client = serializers.CharField()

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class BranchForSubAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Branch
        fields = ('id', 'name',)


class AdminRoleSerializer(serializers.ModelSerializer):
    branch = BranchForSubAdminSerializer()

    class Meta:
        model = models.AdminRole
        fields = ("id", "role", "branch")


class SuperAdminRoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SuperAdminRole
        fields = ("id", "role",)


class UserRegistrationSerializer(serializers.ModelSerializer):
    token = serializers.CharField(required=False, read_only=True)
    username = serializers.CharField()
    userprofile = UserProfileSerializer()

    class Meta:
        model = User
        fields = ('id', 'first_name','password', 'username', 'email', 'token', 'userprofile',)

    @transaction.atomic()
    def create(self,validated_data):
        user_profile = validated_data.pop("userprofile")
        user = User.objects.create_user(**validated_data)
        role = models.Role.objects.get(id=3)
        models.UserRole.objects.create(user=user, role=role)
        models.UserProfile.objects.create(user=user, **user_profile)
        Thread(target=utils.SendOneTimePassword.send_otp, args=(user,)
               ).start()
        return user

    @staticmethod
    def validate_email(attrs):
        if get_object_or_None(User, email=attrs):
            raise serializers.ValidationError(constants.EMAIL_ALREADY_EXIST_ERROR)
        return attrs

    @staticmethod
    def validate_username(value):
        try:
            User.objects.get(username=value)
        except ObjectDoesNotExist:
            return value
        else:
            raise exceptions.ValidationError(constants.NUMBER_ALREADY_EXIST_ERROR)


class AdminRegistrationSerializer(serializers.ModelSerializer):
    token = serializers.CharField(required=False, read_only=True)
    username = serializers.CharField()
    adminrole = AdminRoleSerializer()

    class Meta:
        model = User
        fields = ('id', 'first_name','password', 'username', 'email', 'token',
                  'adminrole',)

    @transaction.atomic()
    def create(self,validated_data):
        admin_role = validated_data.pop("adminrole")
        user = User.objects.create_user(**validated_data)
        models.AdminRole.objects.create(user=user, **admin_role)
        return user

    @staticmethod
    def validate_email(attrs):
        if get_object_or_None(User, email=attrs):
            raise serializers.ValidationError(constants.EMAIL_ALREADY_EXIST_ERROR)
        return attrs

    @staticmethod
    def validate_username(value):
        try:
            User.objects.get(username=value)
        except ObjectDoesNotExist:
            return value
        else:
            raise exceptions.ValidationError(constants.NUMBER_ALREADY_EXIST_ERROR)


class SuperAdminRegistrationSerializer(serializers.ModelSerializer):
    token = serializers.CharField(required=False, read_only=True)
    username = serializers.CharField()
    superadminrole = SuperAdminRoleSerializer()

    class Meta:
        model = User
        fields = ('id', 'first_name','password', 'username', 'email', 'token',
                  'superadminrole',)

    @transaction.atomic()
    def create(self,validated_data):
        superadminrole = validated_data.pop("superadminrole")
        user = User.objects.create_user(**validated_data)
        models.SuperAdminRole.objects.create(user=user, **superadminrole)
        return user

    @staticmethod
    def validate_email(attrs):
        if get_object_or_None(User, email=attrs):
            raise serializers.ValidationError(constants.EMAIL_ALREADY_EXIST_ERROR)
        return attrs

    @staticmethod
    def validate_username(value):
        try:
            User.objects.get(username=value)
        except ObjectDoesNotExist:
            return value
        else:
            raise exceptions.ValidationError(constants.NUMBER_ALREADY_EXIST_ERROR)


class ActivationSerializer(serializers.Serializer):
    mobile_number = serializers.CharField(max_length=10)
    otp = serializers.CharField(max_length=6)
    client = serializers.CharField()

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(min_length=6, max_length=30)
    new_password = serializers.CharField(min_length=6, max_length=30)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass



class ForgotPasswordSerializer(serializers.Serializer):
    mobile_number = serializers.CharField(max_length=10)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class ResetPasswordSerializer(serializers.Serializer):
    mobile_number = serializers.CharField(max_length=10)
    otp = serializers.CharField(max_length=6)
    client = serializers.CharField()
    password = serializers.CharField(min_length=6, max_length=30)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass