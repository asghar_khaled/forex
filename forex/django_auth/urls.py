from django.conf.urls import url
from . import views, utils

urlpatterns = [
    url(r'^register/$', views.UserRegistrationView.as_view(), name='register'),
    url(r'^register/admin/$', views.AdminRegistrationView.as_view(), name='register-admin'),
    url(r'^register/super/admin/$', views.SuperAdminRegistrationView.as_view(),
        name='register-super-admin'),
    url(r'^activate/account/$', views.ActivationView.as_view(), name='activation_view'),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^login/super/admin/$', views.LoginSuperAdminView.as_view(), name='login-super-admin'),
    url(r'^login/user/$', views.LoginUserView.as_view(), name='login-user'),
    url(r'^change-password/$', views.ChangePasswordView.as_view(), name='change_password'),
    url(r'^forgot-password/$', views.ForgotPasswordView.as_view(), name='forgot_password'),
    url(r'^forgot-password-branch-admin/$', views.ForgotPasswordBranchAdminView.as_view(),
        name='forgot_password_branch_admin'),
    url(r'^forgot-password-super-admin/$', views.ForgotPasswordSuperAdminView.as_view(),
        name='forgot_password_super_admin'),
    url(r'^reset-password/$', views.ResetPasswordView.as_view(), name='reset_password'),
    url(r'^reset-password-branch-admin/$', views.ResetPasswordBranchAdminView.as_view(),
        name='reset_password_branch_admin'),
    url(r'^reset-password-super-admin/$', views.ResetPasswordSuperAdminView.as_view(),
        name='reset_password_branch_admin'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^logout/branch/admin/$', views.LogoutBranchAdminView.as_view(), name='logout-branch-admin'),
    url(r'^logout/super/admin/$', views.LogoutSuperAdminView.as_view(), name='logout-super-admin'),


]