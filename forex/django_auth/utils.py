import binascii
import os
import random
import urllib.parse
import urllib.request
from rest_framework import exceptions

from . import models


def random_digits(digits):
    lower = 10 ** (digits - 1)
    upper = 10 ** digits - 1
    return random.randint(lower, upper)


class SendOneTimePassword(object):

    @staticmethod
    def send_otp(user):
        one_time_password = random_digits(6)
        values = {
            'uname': "Indiacements",
            'pass' : "FFMCtran",
            'send': "ICCAPS",
            'dest': int(user.username),
            'msg': "%s is your Forex verification code" % one_time_password

        }
        url = "http://api.unicel.in/SendSMS/sendmsg.php"
        postdata = urllib.parse.urlencode(values).encode('utf-8')
        req = urllib.request.Request(url, postdata)
        urllib.request.urlopen(req)
        user.userrole.otp = one_time_password
        user.userrole.save()

    @staticmethod
    def send_otp_branch_admin(user):
        one_time_password = random_digits(6)
        values = {
            'uname': "Indiacements",
            'pass': "FFMCtran",
            'send': "ICCAPS",
            'dest': int(user.username),
            'msg': "%s is your Forex verification code" % one_time_password

        }
        url = "http://api.unicel.in/SendSMS/sendmsg.php"
        postdata = urllib.parse.urlencode(values).encode('utf-8')
        req = urllib.request.Request(url, postdata)
        urllib.request.urlopen(req)
        user.adminrole.otp = one_time_password
        user.adminrole.save()

    @staticmethod
    def send_otp_super_admin(user):
        one_time_password = random_digits(6)
        values = {
            'uname': "Indiacements",
            'pass': "FFMCtran",
            'send': "ICCAPS",
            'dest': int(user.username),
            'msg': "%s is your Forex verification code" % one_time_password

        }
        url = "http://api.unicel.in/SendSMS/sendmsg.php"
        postdata = urllib.parse.urlencode(values).encode('utf-8')
        req = urllib.request.Request(url, postdata)
        urllib.request.urlopen(req)
        user.superadminrole.otp = one_time_password
        user.superadminrole.save()


class ValidationMixin(object):

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            return self.activate(serializer)
        else:
            raise exceptions.ValidationError(serializer.errors)


def get_or_create_token(user, client):
    token, created = models.Token.objects.get_or_create(
        client=client, user=user, defaults={'user': user, 'client': client,
                                            'key': binascii.hexlify(os.urandom(20)).decode()})
    return token
