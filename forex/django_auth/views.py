from rest_framework import generics, response, status, exceptions, permissions
from django.core.exceptions import ObjectDoesNotExist
from . import utils, serializers, constants
from django.contrib.auth import user_logged_in, user_logged_out
from django.contrib.auth.models import User
from django.db.models import Q
from django.db import transaction
from threading import Thread


# Create your views here.

class UserRegistrationView(generics.CreateAPIView):
    """
    use this endpoint to register an user.
    """
    serializer_class = serializers.UserRegistrationSerializer


class AdminRegistrationView(generics.CreateAPIView):
    """
    use this endpoint to create an admin
    """

    serializer_class = serializers.AdminRegistrationSerializer


class SuperAdminRegistrationView(generics.CreateAPIView):
    """
    use this endpoint to create an admin
    """

    serializer_class = serializers.SuperAdminRegistrationSerializer


class ActivationView(utils.ValidationMixin, generics.GenericAPIView):
    """
    use this endpoint to activate an user account.
    """
    serializer_class = serializers.ActivationSerializer

    def activate(self, serializer):
        user = self.get_user()
        if user.userrole.otp != int(serializer.data['otp']):
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_OTP_ERROR]})
        user.is_active = True
        user.userrole.otp = None
        user.save()
        user.userrole.save()
        user.token = utils.get_or_create_token(user, serializer.data["client"]).key
        serializer = serializers.UserRegistrationSerializer(user, context={'request': self.request})
        return response.Response(serializer.data)

    def get_user(self):
        try:
            return User.objects.get(username=self.request.data['mobile_number'])
        except ObjectDoesNotExist:
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_USER_ERROR]})


class LoginView(utils.ValidationMixin, generics.GenericAPIView):
    """
    use this endpoint to login an Sub Admin (obtain authentication token).
    """
    serializer_class = serializers.ObtainTokenSerializer

    def activate(self, serializer):
        user = self.validate_user(serializer)
        token = utils.get_or_create_token(user, serializer.data['client'])
        user_logged_in.send(sender=user.__class__, request=self.request, user=user)
        user.token = token.key
        data = serializers.AdminRegistrationSerializer(user, context={'request': self.request}).data
        return response.Response(data=data)

    @staticmethod
    def validate_user(serializer):
        """ have to do works later"""
        try:
            user = User.objects.get(Q(username=serializer.data['username'])
                                                               |Q(email=serializer.data['username'])
                                                               )
        except ObjectDoesNotExist:
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_USER_ERROR]})

        if user.is_active:
            if user.check_password(serializer.data['password']) and user.adminrole.role.id==2:
                return user
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_CREDENTIALS_ERROR]})
        raise exceptions.ValidationError({"non_field_errors": [constants.INACTIVE_ACCOUNT_ERROR]})


class LoginUserView(utils.ValidationMixin, generics.GenericAPIView):
    """
    use this endpoint to login an Sub Admin (obtain authentication token).
    """
    serializer_class = serializers.ObtainTokenSerializer

    def activate(self, serializer):
        user = self.validate_user(serializer)
        token = utils.get_or_create_token(user, serializer.data['client'])
        user_logged_in.send(sender=user.__class__, request=self.request, user=user)
        user.token = token.key
        data = serializers.UserRegistrationSerializer(user, context={'request': self.request}).data
        return response.Response(data=data)

    @staticmethod
    def validate_user(serializer):
        try:
            user = User.objects.select_related('userrole').get(Q(username=serializer.data['username'])
                                                               |Q(email=serializer.data['username'])
                                                               )
        except ObjectDoesNotExist:
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_USER_ERROR]})

        if user.is_active:
            if user.check_password(serializer.data['password']) and user.userrole.role.id==3:
                return user
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_CREDENTIALS_ERROR]})
        raise exceptions.ValidationError({"non_field_errors": [constants.INACTIVE_ACCOUNT_ERROR]})


class LoginSuperAdminView(utils.ValidationMixin, generics.GenericAPIView):
    """
    use this endpoint to login an Sub Admin (obtain authentication token).
    """
    serializer_class = serializers.ObtainTokenSerializer

    def activate(self, serializer):
        user = self.validate_user(serializer)
        token = utils.get_or_create_token(user, serializer.data['client'])
        user_logged_in.send(sender=user.__class__, request=self.request, user=user)
        user.token = token.key
        data = serializers.SuperAdminRegistrationSerializer(user, context={'request': self.request}).data
        return response.Response(data=data)

    @staticmethod
    def validate_user(serializer):
        """ have to do works later"""
        try:
            user = User.objects.get(Q(username=serializer.data['username'])
                                                               |Q(email=serializer.data['username'])
                                                               )
        except ObjectDoesNotExist:
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_USER_ERROR]})

        if user.is_active:
            if user.check_password(serializer.data['password']) and user.superadminrole.role.id==1:
                return user
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_CREDENTIALS_ERROR]})
        raise exceptions.ValidationError({"non_field_errors": [constants.INACTIVE_ACCOUNT_ERROR]})



class ChangePasswordView(utils.ValidationMixin, generics.GenericAPIView):
    """
    use this endpoint to change password for an user
    """
    serializer_class = serializers.ChangePasswordSerializer

    permission_classes = (
        permissions.IsAuthenticated,
    )

    def activate(self, serializer):
        user = self.request.user
        if not user.check_password(serializer.data['old_password']):
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_PASSWORD_ERROR]})
        if serializer.data['old_password'] == serializer.data['new_password']:
            raise exceptions.ValidationError({"non_field_errors": [constants.PASSWORD_MISMATCH_ERROR]})
        user.set_password(serializer.data['new_password'])
        user.save()
        return response.Response({"data": 'Password updated successfully'})


class ForgotPasswordView(utils.ValidationMixin, utils.SendOneTimePassword, generics.GenericAPIView):
    """
    use this endpoint to send OTP to an user for forgot password
    """
    serializer_class = serializers.ForgotPasswordSerializer

    def activate(self, serializer):
        user = self.get_user()
        Thread(target=self.send_otp, args=(user,)).start()
        return response.Response({"data": "OTP has been sent to your registered mobile number."})

    def get_user(self):
        try:
            return User.objects.get(username=self.request.data['mobile_number'])
        except User.DoesNotExist:
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_USER_ERROR]})


class ForgotPasswordBranchAdminView(utils.ValidationMixin, utils.SendOneTimePassword, generics.GenericAPIView):
    """
    use this endpoint to send OTP to an user for forgot password
    """
    serializer_class = serializers.ForgotPasswordSerializer

    def activate(self, serializer):
        user = self.get_user()
        Thread(target=self.send_otp_branch_admin, args=(user,)).start()
        return response.Response({"data": "OTP has been sent to your registered mobile number."})

    def get_user(self):
        try:
            return User.objects.get(username=self.request.data['mobile_number'])
        except User.DoesNotExist:
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_USER_ERROR]})


class ForgotPasswordSuperAdminView(utils.ValidationMixin, utils.SendOneTimePassword, generics.GenericAPIView):
    """
    use this endpoint to send OTP to an user for forgot password
    """
    serializer_class = serializers.ForgotPasswordSerializer

    def activate(self, serializer):
        user = self.get_user()
        Thread(target=self.send_otp_super_admin, args=(user,)).start()
        return response.Response({"data": "OTP has been sent to your registered mobile number."})

    def get_user(self):
        try:
            return User.objects.get(username=self.request.data['mobile_number'])
        except User.DoesNotExist:
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_USER_ERROR]})


class ResetPasswordView(utils.ValidationMixin, generics.GenericAPIView):
    """
    use this endpoint to reset user password
    """
    serializer_class = serializers.ResetPasswordSerializer

    @transaction.atomic()
    def activate(self, serializer):
        user = self.get_user()
        if user.userrole.otp != int(serializer.data['otp']):
            raise exceptions.ValidationError({"non_field_errors": constants.INVALID_OTP_ERROR})
        user.set_password(serializer.data['password'])
        user.userrole.otp = None
        user.save()
        user.userrole.save()
        user.token = utils.get_or_create_token(user, serializer.data['client']).key
        serializer = serializers.UserRegistrationSerializer(user, context={'request': self.request})
        return response.Response(serializer.data)

    def get_user(self):
        try:
            return User.objects.get(username=self.request.data['mobile_number'])
        except User.DoesNotExist:
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_USER_ERROR]})


class ResetPasswordBranchAdminView(utils.ValidationMixin, generics.GenericAPIView):
    """
    use this endpoint to reset branch admin password
    """
    serializer_class = serializers.ResetPasswordSerializer

    @transaction.atomic()
    def activate(self, serializer):
        user = self.get_user()
        if user.adminrole.otp != int(serializer.data['otp']):
            raise exceptions.ValidationError({"non_field_errors": constants.INVALID_OTP_ERROR})
        user.set_password(serializer.data['password'])
        user.adminrole.otp = None
        user.save()
        user.adminrole.save()
        user.token = utils.get_or_create_token(user, serializer.data['client']).key
        serializer = serializers.AdminRegistrationSerializer(user, context={'request': self.request})
        return response.Response(serializer.data)

    def get_user(self):
        try:
            return User.objects.get(username=self.request.data['mobile_number'])
        except User.DoesNotExist:
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_USER_ERROR]})


class ResetPasswordSuperAdminView(utils.ValidationMixin, generics.GenericAPIView):
    """
    use this endpoint to reset branch admin password
    """
    serializer_class = serializers.ResetPasswordSerializer

    @transaction.atomic()
    def activate(self, serializer):
        user = self.get_user()
        if user.superadminrole.otp != int(serializer.data['otp']):
            raise exceptions.ValidationError({"non_field_errors": constants.INVALID_OTP_ERROR})
        user.set_password(serializer.data['password'])
        user.superadminrole.otp = None
        user.save()
        user.superadminrole.save()
        user.token = utils.get_or_create_token(user, serializer.data['client']).key
        serializer = serializers.SuperAdminRegistrationSerializer(user, context={'request': self.request})
        return response.Response(serializer.data)

    def get_user(self):
        try:
            return User.objects.get(username=self.request.data['mobile_number'])
        except User.DoesNotExist:
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_USER_ERROR]})


class LogoutView(generics.GenericAPIView):
    """
    use this endpoint to logout an user (remove user authentication token).
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    @transaction.atomic()
    def post(self, request):
        if request.auth:
            request.user.userrole.on_work_mode = False
            request.user.userrole.save()
            request.auth.delete()
            user_logged_out.send(sender=request.user.__class__, request=request, user=request.user)
        return response.Response(status=status.HTTP_200_OK)


class LogoutBranchAdminView(generics.GenericAPIView):
    """
    use this endpoint to logout branch admin (remove branch admin authentication)
    """

    permission_classes = (
        permissions.IsAuthenticated,
    )

    @transaction.atomic()
    def post(self, request):
        if request.auth:
            request.user.adminrole.on_work_mode = False
            request.user.adminrole.save()
            request.auth.delete()
            user_logged_out.send(sender=request.user.__class__, request=request, user=request.user)
        return response.Response(status=status.HTTP_200_OK)


class LogoutSuperAdminView(generics.GenericAPIView):
    """
    use this endpoint to logout branch admin (remove branch admin authentication)
    """

    permission_classes = (
        permissions.IsAuthenticated,
    )

    @transaction.atomic()
    def post(self, request):
        if request.auth:
            request.user.superadminrole.on_work_mode = False
            request.user.superadminrole.save()
            request.auth.delete()
            user_logged_out.send(sender=request.user.__class__, request=request, user=request.user)
        return response.Response(status=status.HTTP_200_OK)




