from django.db import models
from django.contrib.auth.models import User
import binascii
import os
from myapp.models import Branch
# Create your models here.


class Role(models.Model):
    name = models.CharField(max_length=254, unique=True)

    class Meta:
        db_table = "role"


class UserRole(models.Model):
    user = models.OneToOneField(User)
    role = models.ForeignKey(Role)
    otp = models.IntegerField(null=True)

    class Meta:
        db_table = "user_role"


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    passport_no = models.CharField(max_length=8)
    aadhaar_no = models.CharField(max_length=12)

    class Meta:
        db_table = "user_profile"


class AdminRole(models.Model):
    user = models.OneToOneField(User)
    role = models.ForeignKey(Role)
    branch = models.ForeignKey(Branch)
    otp = models.IntegerField(null=True)

    class Meta:
        db_table = "admin_role"


class SuperAdminRole(models.Model):
    user = models.OneToOneField(User)
    role = models.ForeignKey(Role)
    otp = models.IntegerField(null=True)

    class Meta:
        db_table = "super_admin_role"


class Token(models.Model):
    key = models.CharField(max_length=40, primary_key=True)
    user = models.ForeignKey(User, related_name='auth_tokens')
    client = models.CharField(max_length=255)
    device_token = models.TextField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'multitoken'
        unique_together = ('user', 'client',)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key
        return super(Token, self).save(*args, **kwargs)

    @property
    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __unicode__(self):
        return u'{0} (client: {1})'.format(self.key, self.client)

    LOGIN_FIELDS = (
        'client',
    )

