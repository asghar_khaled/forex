from urllib.parse import urlencode

from django.contrib.sites.shortcuts import get_current_site
from rest_framework import exceptions

from datetime import date

from myapp import models

from forex.settings import PAGE_MAX_SIZE


class PaginationMixin(object):
    def __init__(self):
        self.request = None

    def get_pagination_response(self, qs):
        query_count = qs + """.count()"""
        count = eval(query_count)
        url = 'https' if self.request.is_secure() else 'http' + "://" + get_current_site(
            self.request).name + self.request.path
        kwargs = self.request.query_params.dict()
        if 'page' in kwargs:
            try:
                page = int(kwargs['page'])
                del (kwargs['page'])
            except:
                raise exceptions.ValidationError({'page': ['page is not a valid integer.']})
        else:
            page = 1
        end = page * PAGE_MAX_SIZE
        start = end - PAGE_MAX_SIZE
        qs += """[start:end]"""
        queryset = eval(qs)
        kw = urlencode(kwargs, doseq=True)
        prev_page = page - 1
        next_page = page + 1
        if kwargs:
            amber = '&'
        else:
            amber = ''
        if int(page) == 1 or page > (count / PAGE_MAX_SIZE) + 1:
            prev_url = None
        else:
            prev_url = url + '?' + kw + amber + "page=%s" % str(prev_page)
        if int(end) >= count:
            next_url = None
        else:
            next_url = url + '?' + kw + amber + "page=%s" % str(next_page)
        return count, prev_url, next_url, queryset
