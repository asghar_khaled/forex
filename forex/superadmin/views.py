from rest_framework import generics, response, status, permissions
from myapp import models
from . import serializers, pagination
import datetime, calendar, pdb


from django.shortcuts import render

# Create your views here.

class ListCurrencyView(generics.ListAPIView):

    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializers.ListCurrencySerializer
    queryset = models.Currencies.objects.all()


class ListForexView(pagination.PaginationMixin, generics.GenericAPIView):

    """
    use this endpoint to list forex orders on date
    """

    permission_classes = (
        permissions.IsAuthenticated,
    )

    def post(self, request):
        data = dict()
        try:
            date = request.data['date']
            self.year = date[:4]
            self.month = date[5:7]
            self.day = date[8:]
        except:
            date = datetime.datetime.now()
            #date = datetime.date(year=2017, month=9, day=22)
            self.year = date.year
            self.month = date.month
            self.day = date.day
        query_set = """models.Forex.objects.filter(created_on__year=self.year,
                                                   created_on__month=self.month,
                                                   created_on__day=self.day).filter(
                                                   is_paid=1).order_by('-created_on')"""
        data['count'], data['prev_url'], data['next_url'], query_set = self.get_pagination_response(query_set)
        data['results'] = serializers.ListForexSerializer(query_set, many=True).data
        return response.Response(data)


class ListGraphView(generics.GenericAPIView):

    permission_classes = (
        permissions.IsAuthenticated,
    )

    def post(self, request):
        data = {}
        year = int(request.data['year'])
        month = int(request.data['month'])
        num_days = calendar.monthrange(year, month)[1]
        days = [datetime.date(year, month, day) for day in range(1, num_days + 1)]
        forex = models.Forex.objects.all()
        index = 1
        for day in days:
             qs = forex.filter(created_on__year=day.year, created_on__month=day.month,
                               created_on__day=day.day).filter(is_paid=1)
             data[index] = qs.count()
             index+=1
        return response.Response(data)


class ListGraphYearView(generics.GenericAPIView):

    permission_classes = (
        permissions.IsAuthenticated,
    )

    def post(self, request):
        data = dict()
        year = int(request.data['year'])
        months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        orders = []
        forex = models.Forex.objects.filter(created_on__year=year)
        for month in months:
            qs = forex.filter(created_on__month=month).filter(is_paid=1)
            count = qs.count()
            orders.append(count)

        data.update({'No_of_orders':orders})
        return response.Response(data)


class ListForexSellView(pagination.PaginationMixin, generics.GenericAPIView):
    """
    use this endpoint to list forex orders on date
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def post(self, request):
        try:
            date = request.data['date']
            self.year = date[:4]
            self.month = date[5:7]
            self.day = date[8:]
        except:
            date = datetime.datetime.now()
            self.year = date.year
            self.month = date.month
            self.day = date.day
        data = dict()
        query_set = """models.SellForex.objects.filter(created_on__year=self.year,
                                                   created_on__month=self.month,
                                                   created_on__day=self.day).order_by('-created_on')"""
        data['count'], data['prev_url'], data['next_url'], query_set = self.get_pagination_response(query_set)
        data['results'] = serializers.ListForexSellSerializer(query_set, many=True).data
        return response.Response(data)


class ListMoneyTransferView(pagination.PaginationMixin, generics.GenericAPIView):
    """
    use this endpoint to list money transfer
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def post(self, request):
        try:
            date = request.data['date']
            self.year = date[:4]
            self.month = date[5:7]
            self.day = date[8:]
        except:
            date = datetime.datetime.now()
            self.year = date.year
            self.month = date.month
            self.day = date.day
        data = dict()
        query_set = """models.MoneyTransfer.objects.filter(created_on__year=self.year,
                                                           created_on__month=self.month,
                                                           created_on__day=self.day).order_by('-created_on')"""
        data['count'], data['prev_url'], data['next_url'], query_set = self.get_pagination_response(query_set)
        data['results'] = serializers.ListMoneyTransferSerializer(query_set, many=True).data
        return response.Response(data)


class ListReloadForexView(pagination.PaginationMixin, generics.GenericAPIView):
    """
    use this endpoint to list money transfer
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def post(self, request):
        try:
            date = request.data['date']
            self.year = date[:4]
            self.month = date[5:7]
            self.day = date[8:]
        except:
            date = datetime.datetime.now()
            self.year = date.year
            self.month = date.month
            self.day = date.day
        data = dict()
        query_set = """models.ReloadForex.objects.filter(created_on__year=self.year,
                                                           created_on__month=self.month,
                                                           created_on__day=self.day).order_by('-created_on')"""
        data['count'], data['prev_url'], data['next_url'], query_set = self.get_pagination_response(query_set)
        data['results'] = serializers.ListReloadForexSerializer(query_set, many=True).data
        return response.Response(data)


class ListCurrencyForProduct(generics.GenericAPIView):

    def get(self, request):
        import pdb
        data = dict()
        qs = models.Currencies.objects.all()
        data["live_rates"] = serializers.ListCurrencySerializer(qs, many=True).data

        for obj in qs:
            if (obj.id==8):
                obj.value = round(obj.value + 0.01, 2)
            else:
                obj.value = round(obj.value + 01.00, 2)
        data["card"] = serializers.ListCurrencySerializer(qs, many=True).data

        for obj1 in qs:
            obj1.value = obj1.value
        data["cheque"] = serializers.ListCurrencySerializer(qs, many=True).data

        for obj2 in qs:
            if (obj2.id==8):
                obj2.value = round(obj2.value + 0.01, 2)
            else:
                obj2.value = obj2.value + 00.50

        data["cash"] = serializers.ListCurrencySerializer(qs, many=True).data
        return response.Response(data)

