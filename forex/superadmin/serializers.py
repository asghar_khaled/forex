from rest_framework import serializers
from myapp import models



class ListCurrencySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Currencies
        fields = ('id', 'name', 'value','code' )


class ListUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.User
        fields = ('first_name','username' )


class ListProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Product
        fields = ('id', 'name',)


class ListBranchSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Branch
        fields = ('id', 'name')


class ListForexDetails(serializers.ModelSerializer):

    class Meta:
        model = models.ForexDetails
        fields = ('id', 'is_contacted', 'is_business')


class ListForexSerializer(serializers.ModelSerializer):
    user = ListUserSerializer()
    branch = ListBranchSerializer()
    currency = ListCurrencySerializer()
    forexdetails = ListForexDetails()
    product = ListProductSerializer()

    class Meta:
        model = models.Forex
        fields = ('id','user','branch', 'created_on', 'product',
                  'foreign_amount', 'inr_amount','full_amount', 'currency', 'forexdetails',)


class ListForexSellSerializer(serializers.ModelSerializer):
    user = ListUserSerializer()
    currency = ListCurrencySerializer()
    branch = ListBranchSerializer()


    class Meta:
        model = models.SellForex
        fields = ('id','user', 'foreign_amount', 'inr_amount','currency', 'branch')


class ListMoneyTransferSerializer(serializers.ModelSerializer):
    branch = ListBranchSerializer()

    class Meta:
        model = models.MoneyTransfer
        fields = ('id', 'mobile', 'branch','name',)


class ListReloadForexSerializer(serializers.ModelSerializer):
    branch = ListBranchSerializer()

    class Meta:
        model = models.ReloadForex
        fields = ('id', 'mobile','branch','name',)

