from rest_framework import serializers
from myapp import models


class ListCurrencySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Currencies
        fields = ('id', 'name', 'value','code' )


class ListUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.User
        fields = ('first_name','username' )


class ListProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Product
        fields = ('id', 'name',)


class ListForexSerializer(serializers.ModelSerializer):
    user = ListUserSerializer()
    currency = ListCurrencySerializer()
    product = ListProductSerializer()

    class Meta:
        model = models.Forex
        fields = ('id','user' ,'product', 'travel_date', 'inr_amount','full_amount', 'foreign_amount', 'payment_method',
                  'payment_place', 'is_paid', 'branch', 'currency', 'purpose',
                  'travellers','created_on')

class ListForexDetails(serializers.ModelSerializer):

    class Meta:
        model = models.ForexDetails
        fields = ('id', 'is_contacted', 'is_business')


class ListCompletedForexSerializer(serializers.ModelSerializer):
    user = ListUserSerializer()
    currency = ListCurrencySerializer()
    product = ListProductSerializer()
    forexdetails = ListForexDetails()

    class Meta:
        model = models.Forex
        fields = ('id','user' ,'product', 'forexdetails', 'travel_date', 'inr_amount','full_amount', 'foreign_amount', 'payment_method',
                  'payment_place', 'is_paid', 'branch', 'currency', 'purpose',
                  'travellers','created_on')


class ListForexSellSerializer(serializers.ModelSerializer):
    user = ListUserSerializer()
    currency = ListCurrencySerializer()
    product = ListProductSerializer()

    class Meta:
        model = models.SellForex
        fields = ('id','user', 'foreign_amount', 'inr_amount','currency', 'product')


class ListMoneyTransferSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.MoneyTransfer
        fields = ('id', 'mobile', 'branch','name',)


class CreateForexDetailsSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ForexDetails
        fields = ('forex','is_contacted','is_business',)

    def create(self, validated_data):
        is_contacted = validated_data.pop("is_contacted")
        is_business = validated_data.pop("is_business")
        fx =validated_data['forex']
        fx.forex_details_completed = 1
        fx.save()
        fx_details = models.ForexDetails.objects.create(forex=fx, is_contacted=is_contacted,
                                                        is_business=is_business)
        return fx_details


class ListReloadForexSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ReloadForex
        fields = ('id', 'mobile','branch','name',)