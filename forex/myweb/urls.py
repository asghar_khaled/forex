from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^list/currency/$', views.ListCurrencyView.as_view(), name='list-currency'),
    url(r'^list/currency/product/$', views.ListCurrencyForProduct.as_view(),
        name='list-currency-product'),
    url(r'^list/forex/$', views.ListForexView.as_view(), name='list-forex'),
    url(r'^list/completed/forex/$', views.ListCompletedForexView.as_view(),
        name='list-completed-forex'),
    url(r'^list/graph/$', views.ListGraphView.as_view(), name='list-graph'),
    url(r'^list/graph/year/$', views.ListGraphYearView.as_view(), name='list-graph-year'),
    url(r'^list/forex/sell/$', views.ListForexSellView.as_view(), name='list-forex-sell'),
    url(r'^list/money/transfer/$', views.ListMoneyTransferView.as_view(), name='list-money-transfer'),
    url(r'^create/forex/details/$', views.CreateForexDetailsView.as_view(), name='create-forex-details'),
    url(r'^list/reload/forex/$', views.ListReloadForexView.as_view(), name='list-reload-forex'),

]