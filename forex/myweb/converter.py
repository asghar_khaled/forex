from yahoo_finance import Currency
import time
from myapp import models
from rest_framework import generics, exceptions


def scheduler_function():
    print("Scheduling..")
    inr = Currency('INR')
    inr_rate = round(float(inr.get_rate()), 2)
    time.sleep(5)
    usd = Currency('USD')
    usd_rate = round(float(usd.get_rate()), 2)
    print(usd_rate, inr_rate)
    if usd_rate and inr_rate is not None:
        eq_inr_countries = dict()
        eq_inr_countries['USD'] = float(usd_rate * inr_rate)
        codes = ['GBP', 'EUR', 'AUD', 'CAD', 'AED', 'SGD', 'JPY', 'MYR', 'HKD', 'SAR']
        val_dict = dict()
        for code in codes:
            try:
                time.sleep(5)
                curr_code = Currency(code)
                rate = curr_code.get_rate()
                val_dict[code] = float(rate)
            except Exception as e:
                print(e)
                continue

        for key, value in val_dict.items():
            val = (usd_rate/value) * inr_rate
            eq_inr_countries[key] = round(val, 2)

        print(eq_inr_countries)

        for key,value in eq_inr_countries.items():
            qs = models.Currencies.objects.get(code=key)
            print(qs)
            qs.value = value
            qs.save()
    else:
        scheduler_function()

